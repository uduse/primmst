#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <set>
#include <vector>
#include <map>

using namespace std;

struct Edge
{
	int source;
	int target;
	int dist;
	Edge( int source, int target, int dist ):source( source ), target( target ), dist( dist ) {};
};

typedef pair<const int, Edge> pairEdge;
typedef pair<multimap<const int, Edge>::iterator, multimap<const int, Edge>::iterator> itrPair;

bool operator<( const Edge &lhs, const Edge &rhs )
{
	return lhs.dist < rhs.dist;
}

bool operator<( const pairEdge &lhs, const pairEdge &rhs )
{
	return lhs.second < rhs.second;
}

int _tmain( int argc, _TCHAR* argv[] )
{
	int dist;
	int source;
	int target;
	int numEdges;
	int numNodes;

	//====================================//
	//  Read
	//====================================//

	ifstream input( "edges.txt" );
	input >> numNodes >> numEdges;
	multimap<const int, Edge> graph;
	while ( input >> source >> target >> dist )
	{
		graph.insert( pairEdge( source, Edge( source, target, dist ) ) );
		graph.insert( pairEdge( target, Edge( target, source, dist ) ) );
	}

	//====================================//
	//  Span
	//====================================//

	// Init
	vector<bool> visitTable;
	for ( int i = 0; i < numNodes; i++ )
	{
		visitTable.push_back( false );
	}
	multiset<pairEdge> next;
	multimap<const int, Edge>::iterator itr;

	// visit the first one
	int firstEdge = 1;
	itrPair ret = graph.equal_range( 1 );
	visitTable[0] = true;
	// push all adjacents of first one
	for ( itr = ret.first; itr != ret.second; itr++ )
	{
		next.insert( *itr );
	}

	// recursively solve the problem
	long long sum = 0;
	while ( !next.empty() )
	{
		const Edge *currentEdge = &( next.begin()->second );
		if ( !visitTable[currentEdge->target - 1] ) // target not visited
		{
			// visit
			visitTable[next.begin()->second.target - 1] = true;
			sum += currentEdge->dist;
			ret = graph.equal_range( currentEdge->target );
			for ( itr = ret.first; itr != ret.second; itr++ )
			{
				next.insert( *itr );
			}
		}
		else
		{
			next.erase( next.begin() );
		}
	}

	cout << "sum: " << sum << endl;

	return 0;
}